package csui.advpro2021.tais.core;

import csui.advpro2021.tais.model.Log;
import lombok.Data;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Summary {
    private String month;
    private double jamKerja;
    private double pembayaran;

    public Summary(String month, double jamKerja) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = jamKerja * 350;
    }

    public static Summary getMonthlySummary(Iterable<Log> logs, String month) {
        double totalJamKerja = 0;
        for (Log log: logs) {
            if (log.getStartTime().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals(month)) {
                totalJamKerja += log.countJamKerja();
            }
        }

        return new Summary(month, totalJamKerja);
    }

    public static Iterable<Summary> getAllSummary(Iterable<Log> logs) {
        List<Summary> summaries = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.MONTH, i);
            summaries.add(getMonthlySummary(logs, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH)));
        }
        return summaries;
    }

    public double getJamKerja() {
        return jamKerja;
    }
}
