package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


@Entity
@Table(name = "mahasiswa")
@Data
@NoArgsConstructor
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    @ManyToOne
    @JoinColumn(name = "kode_matkul")
    @JsonBackReference
    private MataKuliah mataKuliah;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "mahasiswa")
    private List<Log> logs;


    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public List<Log> getMonthlyLogs(String month) {
        List<Log> res = new ArrayList<>();
        for (Log log: this.logs) {
            if (log.getStartTime().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals(month)) {
                res.add(log);
            }
        }
        return res;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }
}
