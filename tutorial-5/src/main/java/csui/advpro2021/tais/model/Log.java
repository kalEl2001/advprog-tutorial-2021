package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idLog;

    @Column(name = "startTime")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime startTime;

    @Column(name = "endTime")
    @JsonFormat(pattern="dd-MM-yyyy HH:mm")
    private LocalDateTime endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "logs")
    private Mahasiswa mahasiswa;

    public Log(String start, String end, String deskripsi) {
        this(LocalDateTime.parse(start, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")), LocalDateTime.parse(end, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")), deskripsi);
    }

    public Log(LocalDateTime start, LocalDateTime end, String deskripsi) {
        System.out.println(start + " " + end);
        this.startTime = start;
        this.endTime = end;
        this.deskripsi = deskripsi;
    }

    public double countJamKerja() {
        return Duration.between(startTime, endTime).toSeconds()/3600;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public void setStart(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setEnd(LocalDateTime endTime) { this.endTime = endTime; }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}