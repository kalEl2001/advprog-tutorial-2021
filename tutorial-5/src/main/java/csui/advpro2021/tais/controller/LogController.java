package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import csui.advpro2021.tais.core.Summary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/register/{kodeMatkul}/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Mahasiswa> register(@PathVariable(value = "kodeMatkul") String kodeMatkul, @PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = logService.registerAsdos(kodeMatkul, npm);

        return ResponseEntity.ok(mahasiswa);
    }

    @PostMapping(path = "/create/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> createLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        Log newLog = logService.createLog(npm, log);

        return ResponseEntity.ok(newLog);
    }

    @GetMapping(path = "/get", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Log> getLog(@RequestParam("idlog") int idLog) {
        Log log = logService.getLog(idLog);

        return ResponseEntity.ok(log);
    }

    @PostMapping(path = "/update/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        Log updatedLog = logService.updateLog(idLog, log);

        return (updatedLog == null)? new ResponseEntity(HttpStatus.BAD_REQUEST) : ResponseEntity.ok(updatedLog);
    }


    @DeleteMapping(path = "/delete/{idLog}", produces = {"application/json"})
    public ResponseEntity<HttpStatus> deleteLog(@PathVariable(value = "idLog") int idLog){
        logService.deleteLog(idLog);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/monthly", produces = {"application/json"})
    public ResponseEntity<Iterable<Log>> getMonthlyLog(@RequestParam("npm") String npm, @RequestParam("month") String month){
        Iterable<Log> logs = logService.getMonthlyLog(npm, month);

        return ResponseEntity.ok(logs);
    }

    @GetMapping(path = "/summary", produces = {"application/json"})
    public ResponseEntity<Summary> getMonthlySummary(@RequestParam("npm") String npm, @RequestParam("month") String month){
        Summary summary = logService.getMonthlySummary(npm, month);

        return ResponseEntity.ok(summary);
    }

    @GetMapping(path = "/summaries", produces = {"application/json"})
    public ResponseEntity<Iterable<Summary>>getSummaries(@RequestParam("npm") String npm) {
        Iterable<Summary> summaries= logService.getSummaries(npm);

        return ResponseEntity.ok(summaries);
    }

}
