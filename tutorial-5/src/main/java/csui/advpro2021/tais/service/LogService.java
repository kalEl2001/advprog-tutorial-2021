package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.Summary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.ArrayList;

public interface LogService {
    Mahasiswa registerAsdos(String npm, String kodeMatkul);

    Log createLog(String npm, Log log);

    Log getLog(int idLog);

    int deleteLog(int idLog);

    Iterable<Log> getMonthlyLog(String npm, String month);

    Summary getMonthlySummary(String npm, String month);

    Iterable<Summary> getSummaries(String npm);

    Log updateLog(int idLog, Log log);
}
