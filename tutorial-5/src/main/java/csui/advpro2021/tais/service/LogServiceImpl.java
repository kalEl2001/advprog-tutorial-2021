package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.Summary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    MahasiswaService mahasiswaService;

    @Autowired
    MataKuliahService mataKuliahService;

    @Autowired
    LogRepository logRepository;

    @Autowired
    MahasiswaRepository mahasiswaRepository;

    @Autowired
    MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa registerAsdos(String kodeMatkul, String npm) {
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        mataKuliah.addAsdos(mahasiswa);
        mahasiswa.setMataKuliah(mataKuliah);

        mataKuliahRepository.save(mataKuliah);
        mahasiswaRepository.save(mahasiswa);

        return mahasiswa;
    }

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        log.setMahasiswa(mahasiswa);
        mahasiswa.getLogs().add(log);

        mahasiswaRepository.save(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log getLog(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public int deleteLog(int idLog) {
        logRepository.deleteById(idLog);
        return 200;
    }

    @Override
    public Iterable<Log> getMonthlyLog(String npm, String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        return mahasiswa.getMonthlyLogs(month);
    }

    @Override
    public Summary getMonthlySummary(String npm, String month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        return Summary.getMonthlySummary(mahasiswa.getLogs(),month);
    }

    @Override
    public Iterable<Summary> getSummaries(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);

        return Summary.getAllSummary(mahasiswa.getLogs());
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        Log oldLog = logRepository.findByIdLog(idLog);

        if (oldLog == null) {
            return null;
        }


        oldLog.setStart(log.getStartTime());
        oldLog.setEnd(log.getEndTime());
        oldLog.setDeskripsi(log.getDeskripsi());

        logRepository.save(oldLog);

        return oldLog;
    }
}
