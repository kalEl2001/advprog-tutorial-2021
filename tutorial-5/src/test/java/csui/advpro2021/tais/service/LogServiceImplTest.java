package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.Summary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MataKuliahService mataKuliahService;

    @Mock
    private MahasiswaService mahasiswaService;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;


    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private ArrayList<Log> logs;
    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah(
                "TMK",
                "Test Mata Kuliah",
                "Ilmu Komputer"
        );

        mahasiswa = new Mahasiswa(
                "1906123456",
                "John Doe",
                "john.doe@email.com",
                "1.23",
                "081234567890"
        );
        log = new Log(
                "04-06-2021 12:00",
                "04-06-2021 15:00",
                "Test Log 1"
        );
        logs = new ArrayList<>();
        logs.add(log);
    }

    @Test
    public void testRegisterAsdos() {
        matkul.setAsdos(new ArrayList<>());

        when(mataKuliahService.getMataKuliah(anyString())).thenReturn(matkul);
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);

        logService.registerAsdos(matkul.getKodeMatkul(), mahasiswa.getNpm());

        assertNotEquals(0, matkul.getAsdos().size());

    }

    @Test
    public void testCreateLog() {
        matkul.setAsdos(new ArrayList<>());
        mahasiswa.setLogs(new ArrayList<>());

        when(mataKuliahService.getMataKuliah(anyString())).thenReturn(matkul);
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);

        logService.registerAsdos(matkul.getKodeMatkul(), mahasiswa.getNpm());
        logService.createLog(mahasiswa.getNpm(), log);

        assertNotEquals(0, mahasiswa.getLogs().size());

    }

    @Test
    public void testGetLog() {
        matkul.setAsdos(new ArrayList<>());
        mahasiswa.setLogs(new ArrayList<>());

        when(logRepository.findByIdLog(anyInt())).thenReturn(log);

        assertEquals(log, logService.getLog(log.getIdLog()));
    }

    @Test
    public void testDeleteLog() {
        assertEquals(200, logService.deleteLog(1));
    }

    @Test
    public void testGetMonthlyLog() {
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        mahasiswa.setLogs(logs);

        assertEquals(logs, logService.getMonthlyLog(mahasiswa.getNpm(), "June"));
    }

    @Test
    public void testGetMonthlySummary() {
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        mahasiswa.setLogs(logs);

        assertEquals(log.countJamKerja(), logService.getMonthlySummary(mahasiswa.getNpm(), "June").getJamKerja());
    }

    @Test
    public void testGetSummaries() {
        when(mahasiswaService.getMahasiswaByNPM(anyString())).thenReturn(mahasiswa);
        mahasiswa.setLogs(logs);

        Iterable<Summary> summaries = logService.getSummaries(mahasiswa.getNpm());

        assertNotNull(summaries);
    }

    @Test
    public void testUpdateLog() {
        when(logRepository.findByIdLog(anyInt())).thenReturn(log);

        Log newLog = new Log(
               "04-06-2021 12:00",
                "04-06-2021 15:00",
                "Test Log 2"
        );

        assertEquals(newLog, logService.updateLog(log.getIdLog(), newLog));
    }

    @Test
    public void testUpdateLogWhenOldLogNotAvailable() {
        assertEquals(null, logService.updateLog(1000, log));
    }
}
