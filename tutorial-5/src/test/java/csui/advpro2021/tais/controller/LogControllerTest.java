package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import csui.advpro2021.tais.core.Summary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.when;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Log log;
    private ArrayList<Log> logs;
    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah(
                "TMK",
                "Test Mata Kuliah",
                "Ilmu Komputer"
        );

        mahasiswa = new Mahasiswa(
                "1906123456",
                "John Doe",
                "john.doe@email.com",
                "1.23",
                "081234567890"
        );

        log = new Log(
                "04-06-2021 12:00",
                "04-06-2021 15:00",
                "Test Log 1"
        );

        logs = new ArrayList<>();
        logs.add(log);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testRegisterAsdosMethod() throws Exception {

        when(logService.registerAsdos(anyString(), anyString())).thenReturn(mahasiswa);

        mvc.perform(post("/log/register/" + matkul.getKodeMatkul() + "/" + mahasiswa.getNpm() + "/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(mahasiswa)));
    }

    @Test
    public void testCreateAndUpdateLog() throws Exception {
        when(logService.createLog(anyString(), any(Log.class))).thenReturn(log);
        String jsonBody = mapToJson(log);

        mvc.perform(post("/log/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Test Log 1"));

        Log newLog = new Log(
                "04-06-2021 12:00",
                "04-06-2021 15:00",
                "Test Log 2"
        );

        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(newLog);
        jsonBody = mapToJson(newLog);

        mvc.perform(post("/log/update/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Test Log 2"));
    }

    @Test
    public void testGetLog() throws Exception {
        when(logService.getLog(anyInt())).thenReturn(log);

        mvc.perform(get("/log/get?idlog=" + log.getIdLog()))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Test Log 1"));
    }

    @Test
    public void testDeleteLog() throws Exception {
        when(logService.deleteLog(anyInt())).thenReturn(200);
        mvc.perform(delete("/log/delete/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testGetMonthlyLog() throws Exception {
        when(logService.getMonthlyLog(anyString(), anyString())).thenReturn(logs);
        mvc.perform(get("/log/monthly?npm=" + mahasiswa.getNpm() + "&month=April")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(logs)));
    }

    @Test
    public void testGetMonthlySummary() throws Exception {

        Summary summary = new Summary("April", 2);

        when(logService.getMonthlySummary(anyString(), anyString())).thenReturn(summary);
        mvc.perform(get("/log/summary?npm=" + mahasiswa.getNpm() + "&month=April")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(summary)));
    }

    @Test
    public void testGetAllSummary() throws Exception {
        List<Summary> summaries = new ArrayList<>();
        summaries.add(new Summary("April", 1));
        summaries.add(new Summary("Mei", 2));

        when(logService.getSummaries(anyString())).thenReturn(summaries);
        mvc.perform(get("/log/summaries?npm=" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapToJson(summaries)));
    }
}
