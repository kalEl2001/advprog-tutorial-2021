# Requirements

“ Pada dasarnya aplikasi ini membantu pihak fakultas dan mahasiswa dengan menawarkan lowongan pada mata kuliah yang membutuhkan asisten dan membuat mahasiswa dapat melamar pada lowongan yang tersedia. “

+ Terdapat relasi di mana mahasiswa dapat menjadi asisten pada mata kuliah tertentu.

“ Jadi satu mata kuliah dapat menerima banyak mahasiswa? “

“ Benar. Dan mahasiswa hanya dapat menjadi asisten di satu mata kuliah. Definisi table dan beberapa API untuk mahasiswa dan mata kuliah telah berhasil kami buat. Namun, untuk fungsionalitas yang menghubungkan antara mahasiswa dan mata kuliah belum selesai. “

+ Setiap mata kuliah dapat menerima banyak mahasiswa.
+ Setiap mahasiswa hanya bisa menjadi asisten dari satu mata kuliah.
+ Relasi antar mahasiswa dan mata kuliah menjadi one-to-many.

“ Mahasiswa dapat mendaftar ke satu mata kuliah. Untuk implementasi awal, cukup anggap semua yang mendaftar ke suatu lowongan mata kuliah langsung diterima. Mata kuliah yang sudah ada asisten tidak bisa dihapus. Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus juga. “

+ Pendaftaran mahasiswa langsung diterima.
+ ON_DELETE dari mata kuliah dan mahasiswa bersifat RESTRICTED.

“ Selain itu ini.. “ gadis itu berhenti sebentar untuk memperlihatkan dokumen dari model sistem yang ingin dibangun. “ Ada beberapa hal lain yang juga belum diimplementasikan. Setiap mahasiswa bekerja akan dicatat pada sebuah log.  Log merepresentasikan pekerjaan yang dilakukan oleh asisten untuk akhirnya dilaporkan kepada fakultas. Mahasiswa dapat membuat log dan sistem akan menyimpannya. Mahasiswa juga dapat memperbaharui log yang sudah dibuat, dan juga menghapusnya. Selain itu, kita perlu menyiapkan API untuk memperlihatkan memperlihatkan laporan pembayaran. Laporan pembayaran, mahasiswa dapat melihat laporan untuk bulan tertentu. API ini diharapkan mampu mengembalikan sebuah laporan yang berisikan semua pekerjaan asisten pada bulan tersebut dan sebuah summary jumlah uang yang mereka dapatkan.  Pada laporan tersebut diharapkan ada data bulan, jam kerja dengan satuan jam, pembayaran yang didapatkan pada satuan Greil“

+ Mahasiswa dapat membuat log yang akan disimpan oleh sistem.
+ Mahasiswa dapat memperbaharui log yang sudah dibuat.
+ Mahasiswa dapat menghapus log yang sudah dibuat.
+ Mahasiswa dapat melihat laporan pembayaran yang menampilkan semua pekerjaan asisten pada bulan tersebut dan jumlah uang yang mereka dapatkan.
+ Laporan berisi data bulan, jam kerja dalam satuan jam, dan jumlah uang yang didapatkan dalam satuan Greil.

“ Berapa gaji yang mereka dapatkan? “

“ 350 Greil per jam. Sebuah log dapat saja berisikan data lebih singkat dari satu jam. “

+ Rumus jumlah uang yang didapatkan adalah (LamaKerja Jam * 350 Greil/Jam)
+ Tipe data yang menyimpan jam kerja dan jumlah uang yang didapatkan adalah float atau double.

“ Ah soal dokumen itu.. Aku yakin anda sadar bahwa model relasi belum selesai. Semua atribut memang sudah ada di sana, tapi hubungan antar tabel belum aku tuliskan. Apakah tidak masalah anda mencoba mencarinya sendiri? “

“ Tidak masalah. “

“ Ada beberapa cara untuk mendefinisikan relasi berdasarkan definisi tabel. One-to-one, one-to-many, dan many-to-many. Setiap hubungan tersebut sudah didefinisikan pada Spring Boot. “

“ Terima kasih. “

```
Mahasiswa

NPM: String (Primary Key)
Nama : String
Email: String
ipk: String
noTelp: String

MataKuliah

KodeMatkul: String (Primary Key)
namaMatkul: String
prodi: String

Log

idLog: integer(Primary Key, auto increment)
start: datetime
end: datetime
Deskripsi: Text
```

+ Perlu dibuat relasi antara model data.

“ Kami membuat sistem ini dengan REST dengan harapan, consumer dari aplikasi ini dapat beraneka ragam. Kami dapat membuat aplikasi web, desktop, mobile dengan API ini. Jadi apapun yang dapat menerima JSON dapat menggunakan aplikasi ini. Jika ada sistem lain yang ingin menggunakan data kami, desain ini juga fleksibel untuk mengakomodasi hal tersebut. Jadi sebagai sarana komunikasi, sistem sangat ideal. Selanjutnya adalah bagaimana cara mencoba sistem ini. Anda dapat menggunakan Postman atau client lain yang anda biasa gunakan. Cobalah untuk membuat beberapa data untuk mahasiswa dan mata kuliah sebelum memulai. Aku juga menyarankan untuk memperhatikan Request Method pada Controller “

+ Sistem berbentuk REST API.
+ Request Method pada Controller harus sesuai.
